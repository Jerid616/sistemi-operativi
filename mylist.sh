#!/bin/sh
#$0 è il nome del programma, $1 è il case, $2 è la priorità, $3 è la stringa con l'operazione da fare
todo="lista.txt"
case "$1" in
	"add")
		if [ -z "$3" ]
			then
				echo "$2 -1" >> $todo
			else
				echo "$2 $3" >> $todo
		fi;;
	"remove")
		grep -v "$2" $todo > tmp
		mv tmp $todo;;
	"search")
		grep "$2" $todo;;
	"priority")
		grep "$2" $todo;;
		#cat todo | awk -v search="$2" ’$1==search’ fa un mestiere più preciso cercando solo la colonna della priorità
	"allpriorities")
		sort -k1 -r -g $todo;; #ordine numerico!
	"next")
		sort -k1 -g $todo|tail -n1;;
	*)
		echo "no idea";;
esac
